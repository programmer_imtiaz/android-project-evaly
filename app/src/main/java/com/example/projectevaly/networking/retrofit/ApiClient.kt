package com.example.projectevaly.networking.retrofit

import com.example.projectevaly.BuildConfig
import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

class ApiClient {

    companion object{

        var retrofit : Retrofit? = null
        val TIME_OUT_LIMIT : Long = 180

        @Synchronized
        fun getInstance() : Retrofit? = when(retrofit){
            null -> {
                val gson = GsonBuilder().setLenient().create()

                val client = OkHttpClient.Builder().connectTimeout(TIME_OUT_LIMIT, TimeUnit.SECONDS)
                    .writeTimeout(TIME_OUT_LIMIT, TimeUnit.SECONDS)
                    .readTimeout(TIME_OUT_LIMIT, TimeUnit.SECONDS)
                    .build()

                retrofit =  Retrofit.Builder().baseUrl(BuildConfig.BASE_URL).client(client).
                    addConverterFactory(GsonConverterFactory.create(gson)).build()

                retrofit
            }
            else -> retrofit
        }
    }
}