package com.example.projectevaly.networking.retrofit

import com.example.projectevaly.networking.response.CategoryResponse
import com.example.projectevaly.networking.response.ProductListResponse
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiResponse {

    @GET("categories/")
    fun getCategoryList() : Call<List<CategoryResponse>>

    @GET("inventory/products/")
    fun getProductList(@Query("category")   category : String,
                       @Query("page")       page : Int,
                       @Query("limit")      limit : Int = 10) : Call<ProductListResponse>
}