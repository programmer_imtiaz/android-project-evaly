package com.example.projectevaly.networking.response

import com.google.gson.annotations.SerializedName

data class ProductListResponse(

	@field:SerializedName("next")
	val next: String? = null,

	@field:SerializedName("previous")
	val previous: Any? = null,

	@field:SerializedName("count")
	val count: Int? = null,

	@field:SerializedName("results")
	val results: List<ProductItem>? = null
)