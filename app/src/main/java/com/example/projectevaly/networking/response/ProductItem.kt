package com.example.projectevaly.networking.response

import androidx.annotation.NonNull
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName


@Entity(tableName = "products")
data class ProductItem(

	@ColumnInfo(name = "max_price")
	@field:SerializedName("max_price")
	val maxPrice: String? = null,

	@ColumnInfo(name = "min_price")
	@field:SerializedName("min_price")
	val minPrice: String? = null,

	@ColumnInfo(name = "product_image")
	@field:SerializedName("product_image")
	val productImage: String? = null,

	@ColumnInfo(name = "name")
	@field:SerializedName("name")
	val name: String? = null,

	@ColumnInfo(name = "price_type")
	@field:SerializedName("price_type")
	val priceType: String? = null,

	@PrimaryKey
	@ColumnInfo(name = "slug")
	@NonNull
	@field:SerializedName("slug")
	val slug: String,

	@ColumnInfo(name = "cat_slug")
	@NonNull
	val catSlug: String,

	@ColumnInfo(name = "min_discounted_price")
	@field:SerializedName("min_discounted_price")
	val minDiscountedPrice: String? = null
)