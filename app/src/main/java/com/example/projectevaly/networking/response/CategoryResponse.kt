package com.example.projectevaly.networking.response
import androidx.annotation.NonNull
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity(tableName = "category")
data class CategoryResponse(

	@ColumnInfo(name = "image_url")
	@field:SerializedName("image_url")
	val imageUrl: String? = null,

	@ColumnInfo(name = "name")
	@field:SerializedName("name")
	val name: String? = null,

	@PrimaryKey
	@ColumnInfo(name = "slug")
	@NonNull
	@field:SerializedName("slug")
	val slug: String
)