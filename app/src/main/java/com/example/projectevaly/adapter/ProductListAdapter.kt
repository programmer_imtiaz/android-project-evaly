package com.example.projectevaly.adapter

import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.Priority
import com.bumptech.glide.load.resource.bitmap.FitCenter
import com.bumptech.glide.request.RequestOptions
import com.example.projectevaly.R
import com.example.projectevaly.networking.response.CategoryResponse
import com.example.projectevaly.networking.response.ProductItem
import com.example.projectevaly.networking.retrofit.ApiClient
import com.example.projectevaly.utils.inflate
import kotlinx.android.synthetic.main.item_product.view.*

class ProductListAdapter(var productList: MutableList<ProductItem>) : RecyclerView.Adapter<ProductListAdapter.MyViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder = MyViewHolder(parent.inflate(R.layout.item_product))

    override fun getItemCount(): Int = productList.size

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) = holder.bind(productList.get(position))

    inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind(productItem: ProductItem){
            productItem.name?.let { itemView.tvName.text = it }
            val options : RequestOptions = RequestOptions()
                .transform(FitCenter())
                .placeholder(R.drawable.placeholder)
                .error(R.drawable.placeholder)
                .priority(Priority.HIGH)

            Glide.with(itemView.context)
                .load(productItem.productImage)
                .apply(options)
                .into(itemView.productImg)
        }

    }
}