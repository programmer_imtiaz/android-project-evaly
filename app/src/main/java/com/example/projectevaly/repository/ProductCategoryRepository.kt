package com.example.projectevaly.repository

import android.app.Application
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.projectevaly.db.AppDatabase
import com.example.projectevaly.db.dao.CategoryDao
import com.example.projectevaly.networking.response.CategoryResponse
import com.example.projectevaly.networking.retrofit.ApiClient
import com.example.projectevaly.networking.retrofit.ApiResponse
import com.example.projectevaly.utils.Resource
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import kotlin.coroutines.CoroutineContext

class ProductCategoryRepository(application: Application){

    var categoryList : MutableLiveData<Resource<MutableList<CategoryResponse>>>
    var apiResponse : ApiResponse? = null
    var appDB : AppDatabase? = null

    init {
        apiResponse = ApiClient.getInstance()?.create(ApiResponse::class.java)
        categoryList = MutableLiveData()
        appDB = AppDatabase.getInstance(application)
    }

    fun getProductCategoryList() : MutableLiveData<Resource<MutableList<CategoryResponse>>> = categoryList

    fun getProductCategoryListOffline() : LiveData<List<CategoryResponse>> {
        var liveData : LiveData<List<CategoryResponse>> = MutableLiveData()
        appDB?.categoryDao?.getAllCategories()?.let {
            liveData = it
        }
        return liveData
    }

    fun callCategoryListApi(){
        categoryList.value = Resource.loading(null)

        apiResponse?.let {
            val call = it.getCategoryList()
            call.enqueue(object : Callback<List<CategoryResponse>>{
                override fun onResponse(call: Call<List<CategoryResponse>>, response: Response<List<CategoryResponse>>) {
                    categoryList.value = if (response.isSuccessful) Resource.success(response.body()?.toMutableList()) else Resource.error()
                    insertCategoryListToDb(response.body())
                    Log.e("onResponse", "YES")
                }

                override fun onFailure(call: Call<List<CategoryResponse>>, t: Throwable) {
                    Log.e("onFailure", t.toString())
                    categoryList.value = Resource.error()
                }

            })
        }
    }


    fun insertCategoryListToDb(list: List<CategoryResponse>?) {
        list?.let {
             CoroutineScope(Dispatchers.IO).launch {
                 appDB?.categoryDao?.insertCategoryList(it)
             }
        }

    }
}