package com.example.projectevaly.repository

import android.app.Application
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.projectevaly.db.AppDatabase
import com.example.projectevaly.networking.response.CategoryResponse
import com.example.projectevaly.networking.response.ProductItem
import com.example.projectevaly.networking.response.ProductListResponse
import com.example.projectevaly.networking.retrofit.ApiClient
import com.example.projectevaly.networking.retrofit.ApiResponse
import com.example.projectevaly.utils.Resource
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ProductListRepository(application: Application) {

    var productList : MutableLiveData<Resource<MutableList<ProductItem>>>
    var myProductListOffline : LiveData<List<ProductItem>>?
    var apiResponse : ApiResponse? = null
    var appDB : AppDatabase? = null

    init {
        apiResponse = ApiClient.getInstance()?.create(ApiResponse::class.java)
        productList = MutableLiveData()
        myProductListOffline = MutableLiveData()
        appDB = AppDatabase.getInstance(application)
    }

    fun getProductListOnline() : MutableLiveData<Resource<MutableList<ProductItem>>> = productList

    fun getProductListOffline() : LiveData<List<ProductItem>>? = myProductListOffline

    fun callProductListApi(category : String, page : Int){
        productList.value = Resource.loading(null)
        apiResponse?.let {
            val call = it.getProductList(category, page)
            call.enqueue(object : Callback<ProductListResponse>{
                override fun onResponse(call: Call<ProductListResponse>, response: Response<ProductListResponse>) {
                    Log.e("productListCount", response.body()?.count.toString())
                    productList.value = if (response.isSuccessful) Resource.success(response.body()?.results?.toMutableList()) else Resource.error()
                    inserProductListToDb(response.body()?.results, category)
                }

                override fun onFailure(call: Call<ProductListResponse>, t: Throwable) {
                    Log.e("ProductListError", t.toString())
                    productList.value = Resource.error()
                }

            })
        }
    }

    fun callProductListFromDb(category : String){
        myProductListOffline = null
        myProductListOffline = appDB?.productsDao?.getProductList(category)
    }

    fun inserProductListToDb(productList : List<ProductItem>?, category : String){
        productList?.let {
            CoroutineScope(Dispatchers.IO).launch {
                for(i in 0 until it.size){
                    val productObj = it.get(i).copy(catSlug = category)
                    appDB?.productsDao?.insertProduct(productObj)
                }
            }
        }
    }
}