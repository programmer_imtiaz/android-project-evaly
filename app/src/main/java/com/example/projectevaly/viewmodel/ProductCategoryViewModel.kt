package com.example.projectevaly.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.projectevaly.networking.response.CategoryResponse
import com.example.projectevaly.repository.ProductCategoryRepository
import com.example.projectevaly.utils.Resource

class ProductCategoryViewModel(application: Application) : AndroidViewModel(application) {

    var productCategoryRepo : ProductCategoryRepository

    init {
        productCategoryRepo = ProductCategoryRepository(application)
    }

    fun getProductCategoryList() : MutableLiveData<Resource<MutableList<CategoryResponse>>> = productCategoryRepo.getProductCategoryList()

    fun callProductCategoryApi() = productCategoryRepo.callCategoryListApi()

    fun getProductCategoryListOffline() : LiveData<List<CategoryResponse>> = productCategoryRepo.getProductCategoryListOffline()

    override fun onCleared() {
        super.onCleared()
    }
}