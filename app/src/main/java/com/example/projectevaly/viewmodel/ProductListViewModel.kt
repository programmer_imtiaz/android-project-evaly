package com.example.projectevaly.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.projectevaly.networking.response.ProductItem
import com.example.projectevaly.networking.response.ProductListResponse
import com.example.projectevaly.repository.ProductCategoryRepository
import com.example.projectevaly.repository.ProductListRepository
import com.example.projectevaly.utils.Resource

class ProductListViewModel(application: Application) :  AndroidViewModel(application){

    var productListRepo : ProductListRepository

    init {
        productListRepo = ProductListRepository(application)
    }

    fun callProductListApi(category : String, page : Int) = productListRepo.callProductListApi(category, page)

    fun callProductListFromDb(category: String) = productListRepo.callProductListFromDb(category)

    fun getProductListOnline() : MutableLiveData<Resource<MutableList<ProductItem>>> = productListRepo.getProductListOnline()

    fun getProductListOffline() : LiveData<List<ProductItem>>? = productListRepo.getProductListOffline()

    override fun onCleared() {
        super.onCleared()
    }

}