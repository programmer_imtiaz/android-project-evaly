package com.example.projectevaly.utils

import android.app.Application
import android.content.Context
import android.net.ConnectivityManager
import android.os.Build

class App : Application() {

    companion object {

        val CATEGORY = "category"

        fun isNetworkActive(context: Context): Boolean {
            val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            val networkInfo = if(Build.VERSION.SDK_INT < 23) connectivityManager.activeNetworkInfo else connectivityManager.activeNetwork
            return networkInfo != null
        }
    }
}