package com.example.projectevaly.activity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.method.TextKeyListener.clear
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.GridLayoutManager
import com.example.projectevaly.R
import com.example.projectevaly.adapter.CategoryAdapter
import com.example.projectevaly.networking.response.CategoryResponse
import com.example.projectevaly.utils.App
import com.example.projectevaly.utils.Resource
import com.example.projectevaly.viewmodel.ProductCategoryViewModel
import kotlinx.android.synthetic.main.activity_main.*
import java.util.Collections.addAll

class ProductCategoryActivity : AppCompatActivity() {

    lateinit var productCategoryViewModel : ProductCategoryViewModel
    lateinit var categoryList : MutableList<CategoryResponse>
    lateinit var cateogryAdapter : CategoryAdapter

    var networkFailed = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initInstance()
        generateRecyclerView()
        ovserveCategoryListOnline()
        ovserveCategoryListOffline()
    }

    private fun initInstance() {
        productCategoryViewModel = ViewModelProviders.of(this).get(ProductCategoryViewModel::class.java)
        categoryList = ArrayList()
    }

    private fun generateRecyclerView() {
        categoryRecyclerview.apply {
            cateogryAdapter = CategoryAdapter(categoryList){ slug ->
                Log.e("slug", slug)
                Intent(this@ProductCategoryActivity, ProductListActivity::class.java).apply {
                    putExtra("category", slug)
                    startActivity(this)
                }
            }
            layoutManager = GridLayoutManager(applicationContext, 2)
            adapter = cateogryAdapter
        }
        productCategoryViewModel.callProductCategoryApi()
    }

    private fun ovserveCategoryListOnline() {
        productCategoryViewModel.getProductCategoryList().observe(this, object : Observer<Resource<MutableList<CategoryResponse>>> {
            override fun onChanged(resource: Resource<MutableList<CategoryResponse>>?) {
                when(resource?.status){
                    Resource.Status.LOADING -> {
                        Log.e("LOADING", "TRUE")
                        progrssBar.visibility = View.VISIBLE
                        categoryRecyclerview.visibility = View.GONE
                    }
                    Resource.Status.SUCCESS -> {
                        progrssBar.visibility = View.GONE
                        categoryRecyclerview.visibility = View.VISIBLE
                        resource.data?.let {
                            categoryList.apply {
                                clear()
                                addAll(it)
                                cateogryAdapter.notifyDataSetChanged()
                            }
                        }
                    }
                    Resource.Status.ERROR -> {
                        Toast.makeText(applicationContext, "Network Error Occured", Toast.LENGTH_SHORT).show()
                        networkFailed = true
                        ovserveCategoryListOffline()

                    }
                }
            }

        })
    }

    private fun ovserveCategoryListOffline() {
        productCategoryViewModel.getProductCategoryListOffline().observe(this, object : Observer<List<CategoryResponse>>{
            override fun onChanged(list: List<CategoryResponse>?) {
                if(!App.isNetworkActive(applicationContext) || networkFailed){
                    networkFailed = false
                    progrssBar.visibility = View.GONE
                    categoryRecyclerview.visibility = View.VISIBLE
                    list?.let {
                        categoryList.apply {
                            clear()
                            addAll(it)
                            cateogryAdapter.notifyDataSetChanged()
                        }
                    }
                }
            }
        })
    }

}
