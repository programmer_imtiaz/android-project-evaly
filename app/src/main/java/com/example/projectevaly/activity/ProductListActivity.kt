package com.example.projectevaly.activity

import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.projectevaly.R
import com.example.projectevaly.adapter.ProductListAdapter
import com.example.projectevaly.networking.response.ProductItem
import com.example.projectevaly.utils.App
import com.example.projectevaly.utils.Resource
import com.example.projectevaly.viewmodel.ProductListViewModel
import kotlinx.android.synthetic.main.activity_product_list.*

class ProductListActivity : AppCompatActivity() {

    lateinit var productListViewModel : ProductListViewModel
    lateinit var productListAdapter: ProductListAdapter
    lateinit var gridLayoutManager : GridLayoutManager

    var networkConnectionFailed = false
    var pageCount = 1

    val productList : MutableList<ProductItem> by lazy {
        ArrayList<ProductItem>()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_product_list)
        initInstance()
        generateRecyclerView()
        ovserveProductListOnline()
        observeProductListOffline()
        recyclerViewPagingScrollListener()
    }

    fun initInstance(){
        productListViewModel = ViewModelProviders.of(this).get(ProductListViewModel::class.java)
    }

    private fun generateRecyclerView() {
        productListRecyclerview.apply {
            productListAdapter = ProductListAdapter(productList)
            gridLayoutManager = GridLayoutManager(applicationContext, 2)
            layoutManager = gridLayoutManager
            adapter = productListAdapter
        }
        productListViewModel.callProductListApi(intent.getStringExtra(App.CATEGORY), pageCount)
        productListViewModel.callProductListFromDb(intent.getStringExtra(App.CATEGORY))
    }

    private fun recyclerViewPagingScrollListener(){
        productListRecyclerview.addOnScrollListener(object : RecyclerView.OnScrollListener() {

            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                val lastVisibleItem = gridLayoutManager.findLastVisibleItemPosition()
                Log.e("lastVisibleItem", lastVisibleItem.toString())
                val totalItemCount = gridLayoutManager.getItemCount()
                Log.e("totalItemCount", totalItemCount.toString())
                if((lastVisibleItem == totalItemCount - 1) && App.isNetworkActive(applicationContext)){
                    Log.e("AllWatch", "Yes")
                    pageCount ++
                    productListViewModel.callProductListApi(intent.getStringExtra(App.CATEGORY), pageCount)

                }
            }
        })
    }

    private fun ovserveProductListOnline() {
        productListViewModel.getProductListOnline().observe(this, object : Observer<Resource<MutableList<ProductItem>>> {
            override fun onChanged(resource: Resource<MutableList<ProductItem>>?) {
                when(resource?.status){
                    Resource.Status.LOADING -> {
                        Log.e("LOADING", "TRUE")
                        if(pageCount == 1){
                            pbProductList.visibility = View.VISIBLE
                            productListRecyclerview.visibility = View.GONE
                        }
                        else{
                            pbBottom.visibility = View.VISIBLE
                            productListRecyclerview.visibility = View.VISIBLE
                        }

                    }
                    Resource.Status.SUCCESS -> {
                        networkConnectionFailed = false
                        pbBottom.visibility = View.GONE
                        pbProductList.visibility = View.GONE
                        productListRecyclerview.visibility = View.VISIBLE
                        resource.data?.let {
                            productList.apply {
                                addAll(it)
                                productListAdapter.notifyItemRangeInserted(productListAdapter.getItemCount(), it.size - 1)
                            }
                        }
                    }
                    Resource.Status.ERROR -> {
                        Toast.makeText(applicationContext, "Network Error Occured", Toast.LENGTH_SHORT).show()
                        networkConnectionFailed = true
                        productListViewModel.callProductListFromDb(intent.getStringExtra(App.CATEGORY))
                        pbBottom.visibility = View.GONE
                        pbProductList.visibility = View.GONE
                        productListRecyclerview.visibility = View.VISIBLE
                    }
                }
            }

        })
    }

    private fun observeProductListOffline(){
        productListViewModel.getProductListOffline()?.observe(this, object : Observer<List<ProductItem>>{
            override fun onChanged(list: List<ProductItem>?) {
                Log.e("OfflineProductList", "Yes")
                list?.let{
                    if(pageCount == 1 && networkConnectionFailed){
                        networkConnectionFailed = false
                        productList.apply {
                            clear()
                            addAll(it)
                            productListAdapter.notifyDataSetChanged()
                        }
                    }
                }
            }
        })
    }


}
