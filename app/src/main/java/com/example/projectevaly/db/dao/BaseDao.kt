package com.example.projectevaly.db.dao

import androidx.room.*

@Dao
abstract class BaseDao<T> {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insert(obj: T)

    @Update
    abstract fun update(obj: T)

    @Delete
    abstract fun delete(obj : T)
}