package com.example.projectevaly.db.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.projectevaly.networking.response.CategoryResponse

@Dao
abstract class CategoryDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insert(obj: CategoryResponse)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insertCategoryList(list: List<CategoryResponse>)

    @Query("SELECT * FROM category")
    abstract fun getAllCategories(): LiveData<List<CategoryResponse>>
}