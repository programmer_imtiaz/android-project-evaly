package com.example.projectevaly.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.sqlite.db.SupportSQLiteDatabase
import com.example.projectevaly.db.dao.CategoryDao
import com.example.projectevaly.db.dao.ProductsDao
import com.example.projectevaly.networking.response.CategoryResponse
import com.example.projectevaly.networking.response.ProductItem


@Database(entities = [CategoryResponse::class, ProductItem::class], version = 1, exportSchema = false)
abstract class AppDatabase : RoomDatabase() {

    abstract val categoryDao : CategoryDao
    abstract val productsDao : ProductsDao

    companion object {

        val DB_NAME = "ProjectEvaly"
        private var instance : AppDatabase? = null

        val roomCallback = object : RoomDatabase.Callback(){
            override fun onCreate(db: SupportSQLiteDatabase) {
                super.onCreate(db)
            }
        }

        @Synchronized
        fun getInstance(context : Context) : AppDatabase? = when(instance){

            null -> {
                instance = Room.databaseBuilder(context.applicationContext, AppDatabase::class.java, DB_NAME)
                    .fallbackToDestructiveMigration()
                    .addCallback(roomCallback)
                    .build()

                instance
            }

            else -> instance

        }


    }

}