package com.example.projectevaly.db.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.projectevaly.networking.response.ProductItem

@Dao
abstract class ProductsDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insertProduct(productItem: ProductItem)

    @Query("select * from products where cat_slug = :categorySlug limit 1000")
    abstract fun getProductList(categorySlug : String) : LiveData<List<ProductItem>>
}